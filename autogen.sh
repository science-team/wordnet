#!/bin/sh
# WordNet comes with a prebuilded binary database.  When using grind
# you can build the database from plain text files.  Because it is
# the Debian way to use a text source both Grind and WordNet tarball
# are merged into one common tarball.  This script is used to build
# the necessary automake stuff.
#
# Andreas Tille <tille@debian.org>
# License: GPL

     DIE=0

     PROG=wordnet

     (autoconf --version) < /dev/null > /dev/null 2>&1 || {
             echo
             echo "You must have autoconf installed to compile $PROG."
             echo "Download the appropriate package for your distribution,"
             echo "or get the source tarball at ftp://ftp.gnu.org/pub/gnu/"
             DIE=1
     }

     (automake --version) < /dev/null > /dev/null 2>&1 || {
             echo
             echo "You must have automake installed to compile $PROG."
             echo "Get ftp://ftp.gnu.org/pub/gnu/automake-1.3.tar.gz"
             echo "(or a newer version if it is available)"
             DIE=1
     }

     if test "$DIE" -eq 1; then
             exit 1
     fi

     if test -z "$*"; then
             echo "I am going to run ./configure with no arguments - if you wish "
             echo "to pass any to it, please specify them on the $0 command line."
     fi

     for dir in .
     do 
       echo processing $dir
       (cd $dir; \
       aclocalinclude="$ACLOCAL_FLAGS"; \
       aclocal $aclocalinclude; \
       autoheader; automake --add-missing; autoconf)
     done
     ./configure --with-tclconfig=/usr/lib/tcl8.4 --with-tkconfig=/usr/lib/tk8.4 "$@"

     echo 
     echo "Now type 'make' to compile $PROG."
